<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Champion extends Model
{
    public $timestamps = false;
    protected $table = 'champs';
    protected $fillable = [
      'nombre',
      'lore',
      'imagen'
    ];

    function crearAsset ($url){
      return asset($url);
    }
}
