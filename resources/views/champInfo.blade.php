@extends('layouts.app')
@section('content')
  <div class="row mr-1 ml-1">
    <div class="col-12 col-md-5 justify-content-left">
        <header>
          <!--el nombre va con una var para que cambie depende del champ elegido-->
          <h1>RLZ</h1>
        </header>
        <!--el lore va con una var para que cambie depende del champ elegido-->
       <p>Lore</p>
    </div>
    <div class="rounded col-12 col-md-7" alt="Responsive image">
        <!--el asset va con una var para que cambie la imagen depende del champ elegido-->
        <img src="{{ asset('img/morgana.jpg') }}" class="img-fluid">
    </div>
  </div>
@endsection
