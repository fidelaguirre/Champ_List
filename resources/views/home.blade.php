@extends('layouts.app')
@section('content')
  <header class="row justify-content-left">
      <img src="{{ asset('img/morgana.jpg') }}" class="rounded img-fluid col col-md-7" alt="responsive image">
    <div class="col-md-5">
      <h1>LOL DB</h1>
      <form class="form-inline" method="GET" action="{{url('/campeones/Morgana')}}">
        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Lista de Campeones en la base de datos</label>
        <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
          <option selected>Selecciona uno</option>
          <!--Las opciones las cargo desde la base de datos-->
        </select>
        <button type="submit" class="btn btn-primary my-1">Buscar</button>
      </form>
    </div>
  </header>

@endsection
