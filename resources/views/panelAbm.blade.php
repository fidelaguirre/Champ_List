@extends('layouts.app')
@section('content')
<div class="container">
    <h3 class="text-center">ABM DE CAMPEONES</h3>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="home-tab" data-toggle="tab" href="#altaChamps" role="tab" aria-controls="home" aria-selected="true">Alta</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="profile-tab" data-toggle="tab" href="#bajaChamps" role="tab" aria-controls="profile" aria-selected="false">Baja</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="contact-tab" data-toggle="tab" href="#modChamps" role="tab" aria-controls="contact" aria-selected="false">Modificacion</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="altaChamps" role="tabpanel" aria-labelledby="home-tab">
        <form>
            <div class="row">
              <div class="col col-6">
                  <div class="form-group">
                    <label for="formGroupExampleInput">Ingrese el nombre del nuevo campeon</label>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre del campeon">
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlFile1">Seleccione una imagen</label>
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <div class="form-group col col-6">
                  <label for="exampleFormControlTextarea1">Escriba el lore del campeon</label>
                  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Lore del campeon"></textarea>
                </div>
            </div>
          </form>
        </div>
      <div class="tab-pane fade" id="bajaChamps" role="tabpanel" aria-labelledby="profile-tab">
        <form>
          <div class="form-group">
            <label for="exampleFormControlSelect1">Seleccione el nombre del campeon que desea eliminar</label>
              <select class="form-control" id="exampleFormControlSelect1">
              </select>
          </div>
        </form>
      </div>
      <div class="tab-pane fade" id="modChamps" role="tabpanel" aria-labelledby="contact-tab">
        <form>
            <div class="row">
              <div class="col col-6">
                  <div class="form-group">
                    <label for="formGroupExampleInput">Ingrese el nombre del campeon que desea modificar</label>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre del campeon">
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlFile1">Seleccione una imagen</label>
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <div class="form-group col col-6">
                  <label for="exampleFormControlTextarea1">Escriba el lore del campeon</label>
                  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Lore del campeon"></textarea>
                </div>
            </div>
          </form>
      </div>
    </div>
    <hr>
    <h3 class="text-center">ABM DE CUENTAS</h3>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="home-tab" data-toggle="tab" href="#altaUsers" role="tab" aria-controls="home" aria-selected="true">Alta</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="profile-tab" data-toggle="tab" href="#bajaUsers" role="tab" aria-controls="profile" aria-selected="false">Baja</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="contact-tab" data-toggle="tab" href="#modUsers" role="tab" aria-controls="contact" aria-selected="false">Modificación</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="altaUsers" role="tabpanel" aria-labelledby="home-tab">
        <form>
          <div class="row">
            <div class="form-group col col-6">
              <label for="usuario">Ingrese el usuario</label>
              <input type="text" class="form-control" id="usuario" placeholder="Usuario">
              <div class="row">
                <div class="col col-6">
                <label for="apellido">Apellido</label>
                <input type="text" class="form-control" id="apellido" placeholder="Apellido">
              </div>
              <div class="col col-6">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" placeholder="Nombre">
              </div>
              </div>
            </div>
            <div class="col col-6">
              <div class="form-group">
                <label for="Password">Ingrese contraseña</label>
                <input type="password" class="form-control" id="Password" placeholder="********">
              </div>
              <div class="form-group">
                <label for="confirmPassword">Ingrese nuevamente la contraseña</label>
                <input type="password" class="form-control" id="confirmPassword" placeholder="********">
              </div>
            </div>
          </div>
          </form>
      </div>
      <div class="tab-pane fade show" id="bajaUsers" role="tabpanel" aria-labelledby="profile-tab">
        <form>
          <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action">Cras justo odio</a>
            <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
            <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
            <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
            <a href="#" class="list-group-item list-group-item-action">Vestibulum at eros</a>
          </div>
      </form>
      </div>
      <div class="tab-pane fade show" id="modUsers" role="tabpanel" aria-labelledby="contact-tab">
        <form>
          <div class="row">
            <div class="form-group col col-6">
              <label for="usuario">Ingrese el usuario</label>
              <input type="text" class="form-control" id="usuario" placeholder="Usuario">
              <div class="row">
                <div class="col col-6">
                <label for="apellido">Apellido</label>
                <input type="text" class="form-control" id="apellido" placeholder="Apellido">
              </div>
              <div class="col col-6">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" placeholder="Nombre">
              </div>
              </div>
            </div>
            <div class="col col-6">
              <div class="form-group">
                <label for="Password">Ingrese la contraseña actual</label>
                <input type="password" class="form-control" id="Password" placeholder="********">
              </div>
              <div class="form-group">
                <label for="Password">Ingrese la nueva contraseña</label>
                <input type="password" class="form-control" id="Password" placeholder="********">
              </div>
              <div class="form-group">
                <label for="confirmPassword">Ingrese nuevamente la contraseña</label>
                <input type="password" class="form-control" id="confirmPassword" placeholder="********">
              </div>
            </div>
          </div>
      </form>
      </div>
    </div>
</div>
@endsection
